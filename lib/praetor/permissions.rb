require 'jwt'

module Praetor
  class Permissions
    class PermissionError < StandardError
    end
  end
end
