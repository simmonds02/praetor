module Praetor
  class Regex
    KEY_REGEX = /\A[0-9a-z\-_]+\z/i
    EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    US_PHONE_REGEX = /\A\+1[0-9]{10}\z/
    PHONE_REJECT_REGEX = /[^0-9]/
  end
end
