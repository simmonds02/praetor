module Praetor
  module ModelConcerns
    module Permissionable
      extend ActiveSupport::Concern

      def filter_record(restrictable, affordance = :can_view)
        if has_permission(object, affordance)
          restrictable
        end
      end

      def filter_records(restrictable_scope, affordance = :can_view)
        restrictable_class = restrictable_scope.class.to_s.deconstantize.constantize
        restrictable_class_name = restrictable_class.name.split('::').first

        if UserRole.joins(role: [:permissions]).find_by(
          'user_roles.user_roleable': nil,
          'user_roles.owner_key': [nil, key],
          'permissions.restrictable_type': restrictable_class_name,
          "permissions.#{affordance}": true
        )
          return restrictable_scope
        end

        if restrictable_class.method_defined?(:user_roles) || restrictable_class.try(:permission_context_key).present?
          if restrictable_class.permission_context_key
            restrictable_scope = restrictable_scope.left_joins(
              "#{restrictable_class.permission_context_key}": {
                user_roles: {
                  role: [:permissions]
                }
              }
            )
          else
            restrictable_scope = restrictable_scope.left_joins(
              user_roles: {
                role: [:permissions]
              }
            )
          end

          filtered_scope = restrictable_scope.where(
            'user_roles.owner_key': [nil, key],
            'permissions.restrictable_type': restrictable_class_name,
            "permissions.#{affordance}": true
          )
        else
          filtered_scope = restrictable_scope.none
        end

        if restrictable_class.method_defined?(:owner)
          filtered_scope = filtered_scope.or(
            restrictable_scope.where(owner: self)
          )
        end

        filtered_scope.distinct
      end

      def has_permission(restrictable, affordance = :can_view)
        return true if (
          !restrictable.restrictable ||
          restrictable.has_permission_as_owner(self)
        )

        restrictable_class = restrictable.class
        permission_context = restrictable.permission_context
        permission_context_class = permission_context.class unless permission_context.nil?

        if permission_context_class&.name&.include?('::')
          user_roleable_type = "#{permission_context_class.name.deconstantize}::Base"
        else
          user_roleable_type = permission_context_class&.name
        end

        has_permission = nil
        UserRole.where(
          owner: [nil, self],
          user_roleable_type: [nil, user_roleable_type],
          user_roleable_key: [nil, permission_context&.key]
        ).each do |user_role|
          permission = Permission.find_by(
            restrictable_type: restrictable_class.permission_context.name.split('::').first,
            role: user_role.role
          )

          if permission.present?
            permission_value = permission.send(affordance)
            return false if permission_value == false
            has_permission = has_permission || permission_value
          end
        end

        has_permission
      end

      def check_permission(restrictable, affordance = :can_view)
        raise Praetor::Permissions::PermissionError unless has_permission(restrictable, affordance)
      end
    end
  end
end
