module Praetor
  module ModelConcerns
    module PrimaryKey
      extend ActiveSupport::Concern

      module ClassMethods
        def belongs_to_key(association, scope = nil, **options)
          belongs_to association, scope, **options.merge(
            foreign_key: options[:foreign_key] || :"#{association}_key",
            primary_key: :key
          )
          preserve_case association
        end

        def has_one_key(association, scope = nil, **options)
          has_one association, scope, **options.merge(
            foreign_key: options[:foreign_key] || :"#{options[:as] || name.split('::').first.underscore}_key",
            primary_key: :key
          )
        end

        def has_many_key(association, scope = nil, **options)
          has_many association, scope, **options.merge(
            foreign_key: options[:foreign_key] || :"#{options[:as] || name.split('::').first.underscore}_key",
            primary_key: :key
          )
        end
      end
    end
  end
end
