require_relative 'cookie_helpers'

module Praetor
  module ControllerConcerns
    module AuthHelpers
      extend ActiveSupport::Concern
      include Praetor::ControllerConcerns::CookieHelpers

      def auth_user
        auth_user_from_cookie
      end

      def auth_user_from_cookie
        if @auth_user.nil?
          auth_header = request.headers['Authorization']
          if auth_header && auth_header.start_with?('Bearer ')
            cred_token = auth_header[7..-1]
          else
            cred_token = cookies[:cred_token]
          end

          if cred_token.present?
            credentials = Praetor::Auth.decode(cred_token)
            if credentials.present?
              user = User.find_by(id: credentials['id'])
              if user.present? && user.auth_token == credentials['auth_token']
                @auth_user = user
              end
            end
          end

          if @auth_user.nil?
            @auth_user = false
          end
        end

        @auth_user || nil
      end

      def set_auth_cookie(user)
        set_cookie(:cred_token,
          value: Praetor::Auth.encode(
            id: user.id,
            auth_token: user.auth_token
          )
        )
      end

      def delete_auth_cookie
        delete_cookie(:cred_token)
      end
    end
  end
end
