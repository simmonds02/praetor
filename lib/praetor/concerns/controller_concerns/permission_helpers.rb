module Praetor
  module ControllerConcerns
    module PermissionHelpers
      extend ActiveSupport::Concern

      def permission_user
        if @permission_user == nil
          @permission_user = auth_user || User.new
        end

        @permission_user
      end

      def check_permission(restrictable, affordance)
        if !permission_user.has_permission(restrictable, affordance)
          if auth_user.present?
            raise Praetor::Permissions::PermissionError
          else
            raise Praetor::Auth::AuthError
          end
        end
      end
    end
  end
end
