module Praetor
  module ControllerConcerns
    module CookieHelpers
      extend ActiveSupport::Concern

      def set_cookie(key, value:)
        cookies[key] = {
          value: value,
          expires: DateTime.parse('2038-01-01T00:00:00+0000'),
          **cookie_options
        }
      end

      def delete_cookie(key)
        cookies.delete(key, **cookie_options)
      end

      def cookie_options
        {
          httponly: true,
          secure: Rails.application.config.auth['cookies']['secure'],
          same_site: Rails.application.config.auth['cookies']['same_site'],
          domain: Rails.application.config.auth['cookies']['domain']
        }
      end
    end
  end
end
