module Praetor
  class Format
    attr_accessor :scope

    def self.key(string)
      string.gsub(/[^0-9a-z\-_]/i, '_').truncate(32, omission: '')
    end

    def self.interpolate(**options)
      Praetor::Format.new(options[:scope]).interpolate(**options.except(:scope))
    end

    def initialize(scope)
      self.scope = scope
    end

    def interpolate(**options)
      ERB.new(
        File.read("app/views/#{options[:template]}.erb")
      ).result(binding).chomp
    end
  end
end
