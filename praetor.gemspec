Gem::Specification.new do |s|
  s.name = 'praetor'
  s.version = '0.0.45'
  s.summary = 'Praetor'
  s.authors = ['Jamal Simmonds']
  s.email = ['jsimmonds2120@gmail.com']
  s.files = [
    'lib/praetor.rb',
    *Dir[File.join(__dir__, 'lib/{generators,tasks}/**/*')]
  ]
end
